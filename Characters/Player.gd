extends KinematicBody2D

onready var animation = $AnimatedSprite
var SPEED = 100
var GRAVITY = 10
const JUMP_POWER = -250
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var on_ground = false
var jump_count = 0

var is_attacking2 = false
var is_attacking = false
 


export var canDash = false
export var canRoll = false



func _physics_process(delta): 
	
	# Basic movement for left and right
	if Input.is_action_pressed("ui_right") && is_attacking == false :
		velocity.x = SPEED
		animation.flip_h = false
	elif Input.is_action_pressed("ui_left") && is_attacking == false:
		velocity.x = -SPEED
		animation.flip_h = true
	else:
		velocity.x = 0
		if canRoll == true:
			animation.play("Roll")
		elif is_attacking == false:
			animation.play("Idle")
		
		
		
		
	
	#fixing collision place
	if velocity.x > 0 && canRoll == false:
		$Area2D.scale.x = 1
		animation.play("Run")
	elif velocity.x < 0 && canRoll == false:
		$Area2D.scale.x = -1	
		animation.play("Run")
			
	if Input.is_action_just_pressed("Roll") && not is_on_floor() and is_attacking == false and canRoll == false and canDash == false:
		$PLayerCollision.disabled = true
		dash()
	if Input.is_action_just_pressed("Roll") && canRoll == false and is_on_floor() and is_attacking == false:
		get_collision_layer_bit(1) == true
		get_collision_layer_bit(2) == false
		get_collision_mask_bit(1) == true
		get_collision_mask_bit(2) == false
		roll()
		
	

	#jump code function
	if Input.is_action_just_pressed("jump") && canRoll == false and is_attacking == false and canDash == false:
		if jump_count < 1:
			jump_count += 1
			velocity.y = JUMP_POWER
			on_ground = false
			
		
	
			
	if Input.is_action_just_pressed("attack") and is_on_floor() and canRoll == false:
		$Area2D/Swordhit.disabled = false
		is_attacking = true
		animation.play("Attack1")
	
	
		
	
		
	
			
	velocity.y += GRAVITY
		
	if is_on_floor():
		on_ground = true
		jump_count = 0
		canDash = false
	elif not is_on_floor():
		on_ground = false
		if velocity.y < 0:
			animation.play("Jump")
		elif GRAVITY == 0:
			animation.play("Dash")
		elif velocity.y > 0 && canRoll == false:
			animation.play("Fall")
			$PLayerCollision.disabled = false
	
	velocity = move_and_slide(velocity, FLOOR)
		
	

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Attack1":
		is_attacking = false
		$Area2D/Swordhit.disabled = true

	if $AnimatedSprite.animation == "Roll":
		animation.play("Idle")
		canRoll = false
	
func roll():
	is_attacking = false
	canRoll = true
	SPEED = SPEED * 2
	animation.play("Roll")
	$RollTimer.start()
	
	
func dash():
	is_attacking = false
	canDash = true
	SPEED = SPEED * 2
	GRAVITY = 0
	animation.play("Dash")
	velocity.y = 0
	$DashTimer.start()
	
func _on_Area2D_body_entered(body):
	if "Enemy" in body.name:
		body.dead()
	
func _on_RollTimer_timeout():
	get_collision_mask_bit(0) == true
	get_collision_mask_bit(1) == false
	get_collision_layer_bit(0) == true
	get_collision_layer_bit(1) == false
	
	SPEED = 100
	canRoll = false 

func _on_DashTimer_timeout():
	SPEED = 100
	GRAVITY = 10
	
	

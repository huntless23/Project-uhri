extends KinematicBody2D


onready var animation = $AnimatedSprite
var SPEED = 100
var GRAVITY = 10
const JUMP_POWER = -250
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var on_ground = false
var jump_count = 0

var is_attacking2 = false
var is_attacking = false
 

func _physics_process(delta): 
	
	# Basic movement for left and right
	if Input.is_action_pressed("ui_right") && is_attacking == false :
		velocity.x = SPEED
		animation.flip_h = false
	elif Input.is_action_pressed("ui_left") && is_attacking == false:
		velocity.x = -SPEED
		animation.flip_h = true
	else:
		velocity.x = 0
		if is_attacking == false:
			animation.play("Idle")
		
	
		
	
			
	if Input.is_action_just_pressed("attack") and is_on_floor():
		is_attacking = true
		animation.play("Attack1")
	
		
	
		
	
		
	
			
	velocity.y += GRAVITY
		
	if is_on_floor():
		on_ground = true
		jump_count = 0
	elif not is_on_floor():
		on_ground = false
		
	
	
	velocity = move_and_slide(velocity, FLOOR)
		
	

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Attack1":
		is_attacking = false
	
	if $AnimatedSprite.animation == "Attack2":
		is_attacking2 = false
		
	
	
func _on_Area2D_body_entered(body):
	if "Enemy" in body.name:
		body.dead()



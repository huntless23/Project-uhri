extends KinematicBody2D

const GRAVITY = 10
const SPEED = 30
const FLOOR =  Vector2(0, -1)

var velocity = Vector2()
onready var animation = $AnimatedSprite


var direction = 1
var is_dead = false

func _ready():
	pass # Replace with function body.
	
func dead():
	is_dead = true
	velocity = Vector2(0, 0)
	animation.play("Death")

func _physics_process(delta):
	if is_dead == false:
		velocity.x = SPEED * direction
		if direction == 1:
			animation.flip_h = false
			animation.play("Run")
		else:
			animation.flip_h = true
			velocity.y += GRAVITY 
	velocity = move_and_slide(velocity, FLOOR)
	

	if is_on_wall():
		direction = direction * -1
		
		
	
